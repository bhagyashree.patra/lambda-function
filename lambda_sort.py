#!/usr/bin/env python3

names = ["Lucky", "Vicky", "Chhunu", "Munu"]

names.sort() # names.sort(key = lambda x: x.lower()) # print(sorted(names))
print(names)

names.sort(key = lambda x: len(x))
print(names)

names.sort(key = lambda x: len(x), reverse = True)
print(names)

names.sort(key = lambda x: x.lower(), reverse = True)
print(names)
