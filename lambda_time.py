#!/usr/bin/env python3

import time

# Lambda function to get the current time
now = lambda: time.strftime("%H:%M:%S")
print("The current time is:", now())

# Lambda function to get the current date
today = lambda: time.strftime("%d/%m/%Y")
print("The current date is:", today())

# Lambda function to get the current date and time
now_and_date = lambda: time.strftime("%d/%m/%Y %H:%M:%S")
print("The current date and time is:", now_and_date())
