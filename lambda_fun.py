#!/usr/bin/env python3

def add(a, b):
    return a + b

sub = lambda a, b: a - b

big = lambda a, b: a if a > b else b

xyz = add(8, 9)
print(xyz)

print(sub(8, 9))
print(big(8, 9))