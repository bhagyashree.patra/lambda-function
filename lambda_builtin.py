#!/usr/bin/env python

import functools

# Lambda function with filter()
# Filter out the even numbers
even_numbers = lambda x: x % 2 == 0
print(list(filter(even_numbers, [1, 2, 3, 4, 5, 6, 7, 8, 9])))

# or
given_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
even_num = lambda x: x % 2 == 0
new_list = list(filter(even_num, given_list))
print(new_list)

# Lambda function with map()
# Map the even numbers to the square of the number
square_even_numbers = lambda x: x ** 2
print(list(map(square_even_numbers, [1, 2, 3, 4, 5, 6, 7, 8, 9])))

# Lambda function with reduce()
# Reduce the list to a single value
print(functools.reduce(lambda x, y: x + y, [1, 2, 3, 4, 5, 6, 7, 8, 9]))
